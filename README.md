# termynal-react
## termnyal-react is a React wrapper around the amazing library *termynal*

I'll link to that eventually

There are three components

### Window
Window creates a terminal-looking window to put your Termynal.

```jsx
import {Window} from "termynal-react";

function MyTerm(){
    return (
        <Window>
            Hello
        </Window>
    );
}
```

### Terminal
Terminal creates a Termynal instance.

```jsx
import {Window, Terminal} from "termynal-react";

function MyTerm(){
    return (
        <Window>
            <Terminal>
                Egg
            </Terminal>
        </Window>
    );
}
```

### Line
Line creates a Termynal line.

```jsx
import {Window, Terminal, Line} from "termynal-react";

function MyTerm(){
    return (
        <Window>
            <Terminal>
                <Line>Egg</Line>
            </Terminal>
        </Window>
    );
}
```

Line has different types.

| Type | What it does |
| ------ | ------ |
| input | Types out whatever is in the tag |
| progress | creates a progress bar |
| "" | Specifying no type just displays text |

You can figure out the rest